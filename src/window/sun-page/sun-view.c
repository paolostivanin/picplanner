/*
 * sun-view.c
 * Copyright (C) 2021 Zwarf <zwarf@mail.de>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


#include "sun-view.h"
#include <glib/gi18n.h>


struct _PicplannerSun
{
  GtkBox parent_instance;

  GtkWidget *label_morning_rise_time;
  GtkWidget *label_morning_rise_azimuth;

  GtkWidget *label_upper_time;
  GtkWidget *label_upper_azimuth;
  GtkWidget *label_upper_elevation;

  GtkWidget *label_lower_time;
  GtkWidget *label_lower_azimuth;
  GtkWidget *label_lower_elevation;

  GtkWidget *label_evening_set_time;
  GtkWidget *label_evening_set_azimuth;

  GtkWidget *label_morning_dark;
  GtkWidget *label_morning_golden;
  GtkWidget *label_morning_blue;

  GtkWidget *label_evening_golden;
  GtkWidget *label_evening_blue;
  GtkWidget *label_evening_dark;
};

G_DEFINE_TYPE (PicplannerSun, picplanner_sun, GTK_TYPE_BOX)


/*
 * Set the index of the sunrise, upper culmination, sunset and lowest culmination
 * to present the information in the sun view.
 */
void
picplanner_sun_set_rise_upper_set (PicplannerSun *sun,
                                   GDateTime *date_time,
                                   double *coordinates_array,
                                   int *index_rise_upper_set_lower)
{
  char *char_morning_rise_time;
  char *char_morning_rise_azimuth;

  char *char_upper_time;
  char *char_upper_azimuth;
  char *char_upper_elevation;

  char *char_lower_time;
  char *char_lower_azimuth;
  char *char_lower_elevation;

  char *char_evening_set_time;
  char *char_evening_set_azimuth;

  GDateTime *date_time_rise;
  GDateTime *date_time_upper;
  GDateTime *date_time_set;
  GDateTime *date_time_lower;


  date_time_rise = g_date_time_add_minutes (date_time,
                                            index_rise_upper_set_lower[0]*24*60/NUM_DATA_POINTS-12*60);
  date_time_upper = g_date_time_add_minutes (date_time,
                                             index_rise_upper_set_lower[1]*24*60/NUM_DATA_POINTS-12*60);
  date_time_set = g_date_time_add_minutes (date_time,
                                           index_rise_upper_set_lower[2]*24*60/NUM_DATA_POINTS-12*60);
  date_time_lower = g_date_time_add_minutes (date_time,
                                             index_rise_upper_set_lower[3]*24*60/NUM_DATA_POINTS-12*60);

  /*
   * Morning
   */
  if (index_rise_upper_set_lower[0]>1)
    {
      char_morning_rise_time = g_strdup_printf ("%02d:%02d",
                                                g_date_time_get_hour (date_time_rise),
                                                g_date_time_get_minute (date_time_rise));
      char_morning_rise_azimuth = g_strdup_printf ("%s: %.0f\u00B0", _("Azimuth"),
                                                   coordinates_array[index_rise_upper_set_lower[0]*2]);
    }
  else
    {
      char_morning_rise_time = g_strdup_printf ("--:--");
      char_morning_rise_azimuth = g_strdup_printf ("%s: -\u00B0", _("Azimuth"));
    }

  /*
   * Upper Culmination
   */
  char_upper_time = g_strdup_printf ("%02d:%02d",
                                     g_date_time_get_hour (date_time_upper),
                                     g_date_time_get_minute (date_time_upper));
  char_upper_azimuth = g_strdup_printf ("%s: %.0f\u00B0", _("Azimuth"),
                                        coordinates_array[index_rise_upper_set_lower[1]*2]);
  char_upper_elevation = g_strdup_printf ("%s: %.0f\u00B0", _("Elevation"),
                                          coordinates_array[index_rise_upper_set_lower[1]*2+1]);

  /*
   * Evening
   */
  if (index_rise_upper_set_lower[2]>0)
    {
      char_evening_set_time = g_strdup_printf ("%02d:%02d",
                                               g_date_time_get_hour (date_time_set),
                                               g_date_time_get_minute (date_time_set));
      char_evening_set_azimuth = g_strdup_printf ("%s: %.0f\u00B0", _("Azimuth"),
                                                  coordinates_array[index_rise_upper_set_lower[2]*2]);
    }
  else
    {
      char_evening_set_time = g_strdup_printf ("--:--");
      char_evening_set_azimuth = g_strdup_printf ("%s: -\u00B0", _("Azimuth"));
    }

  /*
   * Lower Culmination
   */
  char_lower_time = g_strdup_printf ("%02d:%02d",
                                     g_date_time_get_hour (date_time_lower),
                                     g_date_time_get_minute (date_time_lower));
  char_lower_azimuth = g_strdup_printf ("%s: %.0f\u00B0", _("Azimuth"),
                                        coordinates_array[index_rise_upper_set_lower[3]*2]);
  char_lower_elevation = g_strdup_printf ("%s: %.0f\u00B0", _("Elevation"),
                                          coordinates_array[index_rise_upper_set_lower[3]*2+1]);

  gtk_label_set_text (GTK_LABEL (sun->label_morning_rise_time), char_morning_rise_time);
  gtk_label_set_text (GTK_LABEL (sun->label_morning_rise_azimuth), char_morning_rise_azimuth);

  gtk_label_set_text (GTK_LABEL (sun->label_upper_time), char_upper_time);
  gtk_label_set_text (GTK_LABEL (sun->label_upper_azimuth), char_upper_azimuth);
  gtk_label_set_text (GTK_LABEL (sun->label_upper_elevation), char_upper_elevation);

  gtk_label_set_text (GTK_LABEL (sun->label_evening_set_time), char_evening_set_time);
  gtk_label_set_text (GTK_LABEL (sun->label_evening_set_azimuth), char_evening_set_azimuth);

  gtk_label_set_text (GTK_LABEL (sun->label_lower_time), char_lower_time);
  gtk_label_set_text (GTK_LABEL (sun->label_lower_azimuth), char_lower_azimuth);
  gtk_label_set_text (GTK_LABEL (sun->label_lower_elevation), char_lower_elevation);


  /*
   * Free allocated memory.
   */

  g_date_time_unref (date_time_rise);
  g_date_time_unref (date_time_upper);
  g_date_time_unref (date_time_set);

  g_free (char_morning_rise_time);
  g_free (char_morning_rise_azimuth);
  g_free (char_upper_time);
  g_free (char_upper_azimuth);
  g_free (char_upper_elevation);
  g_free (char_lower_time);
  g_free (char_lower_azimuth);
  g_free (char_lower_elevation);
  g_free (char_evening_set_time);
  g_free (char_evening_set_azimuth);
}


/*
 * Set the index of the dark night, blue hour and golden hour
 * to present the information in the sun view.
 */
void
picplanner_sun_set_dark_blue_golden (PicplannerSun *sun,
                                     GDateTime *date_time,
                                     int *index_dark_blue_golden)
{
  char *char_dark_night_morning;
  char *char_blue_hour_morning;
  char *char_golden_hour_morning;
  GDateTime *dark_night_morning_end;
  GDateTime *blue_hour_morning_begin;
  GDateTime *blue_hour_golden_hour_morning_switch;
  GDateTime *golden_hour_morning_end;

  char *char_golden_hour_evening;
  char *char_blue_hour_evening;
  char *char_dark_night_evening;
  GDateTime *golden_hour_evening_begin;
  GDateTime *golden_hour_blue_hour_evening_switch;
  GDateTime *blue_hour_evening_end;
  GDateTime *dark_night_evening_begin;

  dark_night_morning_end = g_date_time_add_minutes (date_time,
                                                index_dark_blue_golden[0]*24*60/NUM_DATA_POINTS-12*60);
  blue_hour_morning_begin = g_date_time_add_minutes (date_time,
                                                index_dark_blue_golden[1]*24*60/NUM_DATA_POINTS-12*60);
  blue_hour_golden_hour_morning_switch = g_date_time_add_minutes (date_time,
                                                index_dark_blue_golden[2]*24*60/NUM_DATA_POINTS-12*60);
  golden_hour_morning_end = g_date_time_add_minutes (date_time,
                                                index_dark_blue_golden[3]*24*60/NUM_DATA_POINTS-12*60);

  golden_hour_evening_begin = g_date_time_add_minutes (date_time,
                                                index_dark_blue_golden[4]*24*60/NUM_DATA_POINTS-12*60);
  golden_hour_blue_hour_evening_switch = g_date_time_add_minutes (date_time,
                                                index_dark_blue_golden[5]*24*60/NUM_DATA_POINTS-12*60);
  blue_hour_evening_end = g_date_time_add_minutes (date_time,
                                                index_dark_blue_golden[6]*24*60/NUM_DATA_POINTS-12*60);
  dark_night_evening_begin = g_date_time_add_minutes (date_time,
                                                index_dark_blue_golden[7]*24*60/NUM_DATA_POINTS-12*60);

  /*
   * Morning events
   */
  if (index_dark_blue_golden[0]>0)
    {
      char_dark_night_morning = g_strdup_printf ("%02d:%02d",
                                                 g_date_time_get_hour (dark_night_morning_end),
                                                 g_date_time_get_minute (dark_night_morning_end));
    }
  else
    {
      char_dark_night_morning = g_strdup_printf ("--:--");
    }


  if (index_dark_blue_golden[1]>0)
    {
      char_blue_hour_morning = g_strdup_printf ("%02d:%02d -",
                                                g_date_time_get_hour (blue_hour_morning_begin),
                                                g_date_time_get_minute (blue_hour_morning_begin));
    }
  else
    {
      char_blue_hour_morning = g_strdup_printf ("--:-- -");
    }


  if (index_dark_blue_golden[2]>0)
    {
      char_blue_hour_morning = g_strdup_printf ("%s %02d:%02d",
                                                char_blue_hour_morning,
                                                g_date_time_get_hour (blue_hour_golden_hour_morning_switch),
                                                g_date_time_get_minute (blue_hour_golden_hour_morning_switch));
      char_golden_hour_morning = g_strdup_printf ("%02d:%02d -",
                                                  g_date_time_get_hour (blue_hour_golden_hour_morning_switch),
                                                  g_date_time_get_minute (blue_hour_golden_hour_morning_switch));
    }
  else
    {
      char_blue_hour_morning = g_strdup_printf ("%s --:--", char_blue_hour_morning);
      char_golden_hour_morning = g_strdup_printf ("--:-- -");
    }


  if (index_dark_blue_golden[3]>0)
    {
      char_golden_hour_morning = g_strdup_printf ("%s %02d:%02d",
                                                  char_golden_hour_morning,
                                                  g_date_time_get_hour (golden_hour_morning_end),
                                                  g_date_time_get_minute (golden_hour_morning_end));
    }
  else
    {
      char_golden_hour_morning = g_strdup_printf ("%s --:--", char_golden_hour_morning);
    }



  /*
   * Evening events
   */
  if (index_dark_blue_golden[4]>0)
    {
      char_golden_hour_evening = g_strdup_printf ("%02d:%02d -",
                                                  g_date_time_get_hour (golden_hour_evening_begin),
                                                  g_date_time_get_minute (golden_hour_evening_begin));
    }
  else
    {
      char_golden_hour_evening = g_strdup_printf ("--:-- -");

    }


  if (index_dark_blue_golden[5]>0)
    {
      char_golden_hour_evening = g_strdup_printf ("%s %02d:%02d",
                                                  char_golden_hour_evening,
                                                  g_date_time_get_hour (golden_hour_blue_hour_evening_switch),
                                                  g_date_time_get_minute (golden_hour_blue_hour_evening_switch));
      char_blue_hour_evening = g_strdup_printf ("%02d:%02d -",
                                                g_date_time_get_hour (golden_hour_blue_hour_evening_switch),
                                                g_date_time_get_minute (golden_hour_blue_hour_evening_switch));

    }
  else
    {
      char_golden_hour_evening = g_strdup_printf ("%s --:--",
                                                  char_golden_hour_evening);
      char_blue_hour_evening = g_strdup_printf ("--:-- -");

    }


  if (index_dark_blue_golden[6]>0)
    {
      char_blue_hour_evening = g_strdup_printf ("%s %02d:%02d",
                                                char_blue_hour_evening,
                                                g_date_time_get_hour (blue_hour_evening_end),
                                                g_date_time_get_minute (blue_hour_evening_end));

    }
  else
    {
      char_blue_hour_evening = g_strdup_printf ("%s --:--",
                                                char_blue_hour_evening);

    }


  if (index_dark_blue_golden[7]>0)
    {
      char_dark_night_evening = g_strdup_printf ("%02d:%02d",
                                                 g_date_time_get_hour (dark_night_evening_begin),
                                                 g_date_time_get_minute (dark_night_evening_begin));
    }
  else
    {
      char_dark_night_evening = g_strdup_printf ("--:--");
    }


  gtk_label_set_text (GTK_LABEL (sun->label_morning_dark), char_dark_night_morning);
  gtk_label_set_text (GTK_LABEL (sun->label_morning_blue), char_blue_hour_morning);
  gtk_label_set_text (GTK_LABEL (sun->label_morning_golden), char_golden_hour_morning);

  gtk_label_set_text (GTK_LABEL (sun->label_evening_golden), char_golden_hour_evening);
  gtk_label_set_text (GTK_LABEL (sun->label_evening_blue), char_blue_hour_evening);
  gtk_label_set_text (GTK_LABEL (sun->label_evening_dark), char_dark_night_evening);


  /*
   * Free allocated memory.
   */
  g_date_time_unref (dark_night_morning_end);
  g_date_time_unref (blue_hour_morning_begin);
  g_date_time_unref (blue_hour_golden_hour_morning_switch);
  g_date_time_unref (golden_hour_morning_end);
  g_date_time_unref (golden_hour_evening_begin);
  g_date_time_unref (golden_hour_blue_hour_evening_switch);
  g_date_time_unref (blue_hour_evening_end);
  g_date_time_unref (dark_night_evening_begin);

  g_free (char_dark_night_morning);
  g_free (char_blue_hour_morning);
  g_free (char_golden_hour_morning);
  g_free (char_golden_hour_evening);
  g_free (char_blue_hour_evening);
  g_free (char_dark_night_evening);
}

static void
picplanner_sun_init (PicplannerSun *self)
{
  gtk_widget_init_template (GTK_WIDGET (self));
}

static void
picplanner_sun_class_init (PicplannerSunClass *class)
{
  gtk_widget_class_set_template_from_resource (GTK_WIDGET_CLASS (class),
                                               "/de/zwarf/picplanner/window/sun-page/sun-view.ui");
  gtk_widget_class_bind_template_child (GTK_WIDGET_CLASS (class), PicplannerSun, label_morning_rise_time);
  gtk_widget_class_bind_template_child (GTK_WIDGET_CLASS (class), PicplannerSun, label_morning_rise_azimuth);
  gtk_widget_class_bind_template_child (GTK_WIDGET_CLASS (class), PicplannerSun, label_upper_time);
  gtk_widget_class_bind_template_child (GTK_WIDGET_CLASS (class), PicplannerSun, label_upper_azimuth);
  gtk_widget_class_bind_template_child (GTK_WIDGET_CLASS (class), PicplannerSun, label_upper_elevation);
  gtk_widget_class_bind_template_child (GTK_WIDGET_CLASS (class), PicplannerSun, label_lower_time);
  gtk_widget_class_bind_template_child (GTK_WIDGET_CLASS (class), PicplannerSun, label_lower_azimuth);
  gtk_widget_class_bind_template_child (GTK_WIDGET_CLASS (class), PicplannerSun, label_lower_elevation);
  gtk_widget_class_bind_template_child (GTK_WIDGET_CLASS (class), PicplannerSun, label_evening_set_time);
  gtk_widget_class_bind_template_child (GTK_WIDGET_CLASS (class), PicplannerSun, label_evening_set_azimuth);
  gtk_widget_class_bind_template_child (GTK_WIDGET_CLASS (class), PicplannerSun, label_morning_dark);
  gtk_widget_class_bind_template_child (GTK_WIDGET_CLASS (class), PicplannerSun, label_morning_blue);
  gtk_widget_class_bind_template_child (GTK_WIDGET_CLASS (class), PicplannerSun, label_morning_golden);
  gtk_widget_class_bind_template_child (GTK_WIDGET_CLASS (class), PicplannerSun, label_evening_golden);
  gtk_widget_class_bind_template_child (GTK_WIDGET_CLASS (class), PicplannerSun, label_evening_blue);
  gtk_widget_class_bind_template_child (GTK_WIDGET_CLASS (class), PicplannerSun, label_evening_dark);
}

PicplannerSun *
picplanner_sun_new ()
{
  return g_object_new (PICPLANNER_SUN_TYPE, NULL);
}
