/*
 * picplanner-window.h
 * Copyright (C) 2021 Zwarf <zwarf@mail.de>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#pragma once

#include <gtk/gtk.h>
#include <adwaita.h>
#include <glib/gi18n.h>
#include <libgweather/gweather.h>
#include "picplanner-config.h"
#include "picplanner-application.h"
#include "search/search.h"
#include "location-service.h"

G_BEGIN_DECLS

#define SEARCH_TIMEOUT_LENGTH 1000

#define PICPLANNER_TYPE_WINDOW picplanner_window_get_type()

G_DECLARE_FINAL_TYPE (PicplannerWindow, picplanner_window, PICPLANNER, WINDOW, AdwApplicationWindow)

PicplannerWindow *picplanner_window_new (PicplannerApplication *app);

void picplanner_window_open (PicplannerWindow *win, GFile *file);

void
picplanner_set_location (double latitude, double longitude, PicplannerWindow *window);

G_END_DECLS
