/*
 * preferences-dialog.c
 * Copyright (C) 2021 Zwarf <zwarf@mail.de>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


/*
 * TODO:
 * TimeZone manual mode?
 */


#include "preferences-dialog.h"


struct _PicplannerPrefs
{
  GtkDialog parent_instance;
  GSettings *settings;
  GtkWidget *timezone;
  GtkWidget *map_unit_metric;
  GtkWidget *map_unit_imperial;
  GtkWidget *invert_scheme;
  GtkWidget *timezone_manual;
  GtkWidget *timezone_row;
};

G_DEFINE_TYPE (PicplannerPrefs, picplanner_prefs, GTK_TYPE_DIALOG)

static void
picplanner_prefs_init (PicplannerPrefs *prefs)
{
  gtk_widget_init_template (GTK_WIDGET (prefs));
  prefs->settings = g_settings_new ("de.zwarf.picplanner");

  g_object_bind_property (prefs->timezone_manual, "active",
                          prefs->timezone_row, "sensitive",
                          G_BINDING_DEFAULT);

  g_settings_bind (prefs->settings, "time-zone",
                   prefs->timezone, "value",
                   G_SETTINGS_BIND_DEFAULT);

  g_settings_bind (prefs->settings, "time-zone-manual",
                   prefs->timezone_manual, "active",
                   G_SETTINGS_BIND_DEFAULT);

  g_settings_bind (prefs->settings, "map-unit-metric",
                   prefs->map_unit_metric, "active",
                   G_SETTINGS_BIND_DEFAULT);

  g_settings_bind (prefs->settings, "map-unit-imperial",
                   prefs->map_unit_imperial, "active",
                   G_SETTINGS_BIND_DEFAULT);

  g_settings_bind (prefs->settings, "invert-scheme",
                   prefs->invert_scheme, "active",
                   G_SETTINGS_BIND_DEFAULT);
}

static void
picplanner_prefs_dispose (GObject *object)
{
  PicplannerPrefs *prefs;

  prefs = PICPLANNER_PREFS (object);

  g_clear_object (&prefs->settings);

  G_OBJECT_CLASS (picplanner_prefs_parent_class)->dispose (object);
}

static void
picplanner_prefs_class_init (PicplannerPrefsClass *class)
{
  G_OBJECT_CLASS (class)->dispose = picplanner_prefs_dispose;

  gtk_widget_class_set_template_from_resource (GTK_WIDGET_CLASS (class),
                                               "/de/zwarf/picplanner/window/preferences-dialog/preferences-dialog.ui");
  gtk_widget_class_bind_template_child (GTK_WIDGET_CLASS (class), PicplannerPrefs, timezone);
  gtk_widget_class_bind_template_child (GTK_WIDGET_CLASS (class), PicplannerPrefs, map_unit_metric);
  gtk_widget_class_bind_template_child (GTK_WIDGET_CLASS (class), PicplannerPrefs, map_unit_imperial);
  gtk_widget_class_bind_template_child (GTK_WIDGET_CLASS (class), PicplannerPrefs, invert_scheme);
  gtk_widget_class_bind_template_child (GTK_WIDGET_CLASS (class), PicplannerPrefs, timezone_manual);
  gtk_widget_class_bind_template_child (GTK_WIDGET_CLASS (class), PicplannerPrefs, timezone_row);
}

PicplannerPrefs *
picplanner_prefs_new (PicplannerWindow *win)
{
  return g_object_new (PICPLANNER_PREFS_TYPE, "transient-for", win, "use-header-bar", TRUE, NULL);
}
