/*
 * calculations_milky_way.c
 * Copyright (C) 2021 Zwarf <zwarf@mail.de>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


#include "calculations_milky_way.h"

/*
 * Get the rotational coordinates of the milky way.
 * Due to the nearly stationary stars, the rotational coordinates are fixed coordinates.
 */
double
*picplanner_get_coordinates_rotational_milky_way (void)
{
  double right_ascension = 266.417;
  double declination = -29.008;
  double *coordinates_rotational = malloc (sizeof (double) * 2);

  coordinates_rotational[0] = right_ascension;
  coordinates_rotational[1] = declination;

  return coordinates_rotational;
}


/*
 * Calculate the horizontal coordinates of the milky way for a given time and position.
 */
double
*picplanner_get_coordinates_milky_way (GDateTime  *date_time,
                                       double     longitude,
                                       double     latitude)
{
  double siderial_time;
  double *coordinates_milky_way;
  double *coordinates_horizontal_milky_way;

  coordinates_milky_way = picplanner_get_coordinates_rotational_milky_way ();
  siderial_time = time_jd_to_sidereal_time (longitude, date_time);
  coordinates_horizontal_milky_way = picplanner_transform_rotational_to_horizontal(coordinates_milky_way,
                                                                                   latitude,
                                                                                   siderial_time);

  g_free (coordinates_milky_way);

  return coordinates_horizontal_milky_way;
}


/*
 * Calculate the horizontal coordinates of the milky way for a given time
 * and position for a whole day.
 */
double
*picplanner_get_array_coordinates_milky_way (GDateTime  *date_time,
                                             double     longitude,
                                             double     latitude)
{
  GDateTime *iteration_time;
  double *coordinates_milky_way;
  double *array_coordinates_milky_way = malloc (sizeof (double) * 2 * NUM_DATA_POINTS);

  for (int i=0; i<NUM_DATA_POINTS; i++)
    {
      iteration_time = g_date_time_add_minutes (date_time, i*24*60/NUM_DATA_POINTS-12*60);

      coordinates_milky_way = picplanner_get_coordinates_milky_way (iteration_time,
                                                                    longitude,
                                                                    latitude);
      array_coordinates_milky_way[2*i] = coordinates_milky_way[0];
      array_coordinates_milky_way[2*i+1] = coordinates_milky_way[1];

      g_free (coordinates_milky_way);
      g_date_time_unref (iteration_time);
    }

  return array_coordinates_milky_way;
}


/*
 * Chencks if the milky way is disturbed by the sun or moon.
 * The definition of "disturbe" can be specified through the angles.
 * 'angle_sun' defines below which angle no disturbance happens by the sun.
 * 'angle_moon' defines below which angle no disturbance happens by the moon.
 * 'angle_milky_way' defines above which angle the milky way is visible.
 */
char
*picplanner_get_char_disturbance (GDateTime *date_time,
                                  double    angle_sun,
                                  double    angle_moon,
                                  double    angle_milky_way,
									                double    *coordinates_array_sun,
									                double    *coordinates_array_moon,
                                  double    *coordinates_array_milky_way)
{
  int       no_disturbance_count    = 0;
  char      *char_no_disturbance    = NULL;
	gboolean  no_disturbance_now      = FALSE;
  GDateTime *date_time_disturbance  = g_date_time_new_now_local ();

	for (int i=0; i<NUM_DATA_POINTS; i++)
		{
			if (coordinates_array_sun[i*2+1]<angle_sun &&
          coordinates_array_moon[i*2+1]<angle_moon   &&
          coordinates_array_milky_way[i*2+1]>angle_milky_way)
				{
					if (!no_disturbance_now)
						{
							no_disturbance_now = TRUE;
							picplanner_get_date_time_from_index (&date_time_disturbance, &date_time, i);
              if (no_disturbance_count == 0)
                {
                  char_no_disturbance = g_strdup_printf ("%02d:%02d -",
																										      g_date_time_get_hour (date_time_disturbance),
																										      g_date_time_get_minute (date_time_disturbance));
                }
              else
                {
                  char_no_disturbance = g_strdup_printf ("%s  &  %02d:%02d -",
                                                         char_no_disturbance,
																										      g_date_time_get_hour (date_time_disturbance),
																										      g_date_time_get_minute (date_time_disturbance));
                }
              no_disturbance_count++;
						}
          else
            {
              if (i==NUM_DATA_POINTS-1)
                {
                  char_no_disturbance = g_strdup_printf ("%s 23:59", char_no_disturbance);
                }
            }
				}
      else
        {
          if (no_disturbance_now)
            {
              no_disturbance_now = FALSE;
              picplanner_get_date_time_from_index (&date_time_disturbance, &date_time, i);
              char_no_disturbance = g_strdup_printf ("%s %02d:%02d",
                                                     char_no_disturbance,
                                                     g_date_time_get_hour (date_time_disturbance),
                                                     g_date_time_get_minute (date_time_disturbance));
            }
        }
		}

  if (char_no_disturbance == NULL)
    char_no_disturbance = g_strdup_printf ("--:-- - --:--");

  g_date_time_unref (date_time_disturbance);

  return char_no_disturbance;
}