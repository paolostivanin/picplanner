/*
 * calculations_milky_way.h
 * Copyright (C) 2021 Zwarf <zwarf@mail.de>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


#include "calculations_transformations.h"


double
*picplanner_get_coordinates_rotational_milky_way ();

double
*picplanner_get_coordinates_milky_way (GDateTime 	*date_time,
                                       double 		longitude,
                                       double 		latitude);

double
*picplanner_get_array_coordinates_milky_way (GDateTime 	*date_time,
                                             double 		longitude,
                                             double 		latitude);

char
*picplanner_get_char_disturbance (GDateTime *date_time,
                                  double    angle_sun,
                                  double    angle_moon,
                                  double    angle_milky_way,
									                double    *coordinates_array_sun,
									                double    *coordinates_array_moon,
                                  double    *coordinates_array_milky_way);
