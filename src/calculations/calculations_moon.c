/*
 * calculations_moon.c
 * Copyright (C) 2021 Zwarf <zwarf@mail.de>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


#include "calculations_moon.h"
#include "calculations_sun.h"

double
*picplanner_get_coordinates_rotational_moon (GDateTime *date_time)
{
  double *coordinates_moon = malloc(sizeof (double) * 2);
  double right_ascension;
  double declination;
  double T;
  double mean_anomaly;
  double mean_longitude; /* ecliptic longitude? */
  double mean_distance;
  double longitude_moon;
  double latitude_moon;
  double ecliptic;
  double time_jd;

  time_jd = calc_jd (date_time);

  T = (time_jd-2451545.0)/36525.;

  mean_longitude = 218.31617 + (13.176396*36525.) * T;
  mean_anomaly   = 134.96292 + (13.064993*36525.) * T;
  mean_distance  = 93.27283  + (13.229350*36525.) * T;

  longitude_moon = mean_longitude + 6.288889 * sin(calc_deg_to_rad (mean_anomaly));
  latitude_moon = 5.128 * sin(calc_deg_to_rad(mean_distance));

  ecliptic = calc_deg_to_rad(23.43928 + 0.01301*T);
  longitude_moon = calc_deg_to_rad (longitude_moon);
  latitude_moon = calc_deg_to_rad (latitude_moon);

  right_ascension = atan2(cos(ecliptic)*sin(longitude_moon)
                          -sin(ecliptic)*tan(latitude_moon), cos(longitude_moon));

  declination = asin(cos(ecliptic)*sin(latitude_moon)
                     +sin(ecliptic)*cos(latitude_moon)*sin(longitude_moon));

  coordinates_moon[0] = calc_rad_to_deg (right_ascension);
  coordinates_moon[1] = calc_rad_to_deg (declination);

  return coordinates_moon;
}


double
picplanner_get_illumination (GDateTime *date_time)
{
  double illumination;
  double elongation;
  double *coordinates_moon;
  double *coordinates_sun;
  double ra_sun, dec_sun, ra_moon, dec_moon;
  coordinates_moon = picplanner_get_coordinates_rotational_moon (date_time);
  coordinates_sun = picplanner_get_coordinates_rotational_sun (date_time);
  ra_sun = calc_deg_to_rad(coordinates_sun[0]);
  dec_sun = calc_deg_to_rad(coordinates_sun[1]);
  ra_moon = calc_deg_to_rad(coordinates_moon[0]);
  dec_moon = calc_deg_to_rad(coordinates_moon[1]);


  elongation = sin(dec_sun)*sin(dec_moon) + cos(dec_sun)*cos(dec_moon)*cos(ra_sun-ra_moon);
  illumination = (1 - elongation)/2;

  g_free (coordinates_moon);
  g_free (coordinates_sun);

  return illumination*100.;
}


double
*picplanner_get_coordinates_moon (GDateTime *date_time,
                                  double    longitude,
                                  double    latitude)
{
  double siderial_time;
  double *coordinates_moon;
  double *coordinates_horizontal_moon;

  coordinates_moon = picplanner_get_coordinates_rotational_moon (date_time);
  siderial_time = time_jd_to_sidereal_time (longitude, date_time);
  coordinates_horizontal_moon = picplanner_transform_rotational_to_horizontal (coordinates_moon,
                                                                               latitude,
                                                                               siderial_time);

  g_free (coordinates_moon);

  return coordinates_horizontal_moon;
}


double
*picplanner_get_array_coordinates_moon (GDateTime *date_time,
                                        double    longitude,
                                        double    latitude)
{
  GDateTime *iteration_time;
  double *coordinates_moon;
  double *array_coordinates_moon = malloc (sizeof (double) * 2 * NUM_DATA_POINTS);

  for (int i=0; i<NUM_DATA_POINTS; i++)
    {
      iteration_time = g_date_time_add_minutes (date_time, i*24*60/NUM_DATA_POINTS-12*60);

      coordinates_moon = picplanner_get_coordinates_moon (iteration_time,
                                                          longitude,
                                                          latitude);
      array_coordinates_moon[2*i] = coordinates_moon[0];
      array_coordinates_moon[2*i+1] = coordinates_moon[1];

      g_free (coordinates_moon);
      g_date_time_unref (iteration_time);
    }

  return array_coordinates_moon;
}
